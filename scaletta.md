# Accessibilità delle applicazioni per dispositivi mobili realizzate con tecnologie di sviluppo multi piattaforma
Tesi di: Niccolò Cantù
Relatore: Sergio Mascetti
Correlatore: Cristian Bernareggi

## Scaletta

1. Introduzione

2. Analisi (descrizione di contesto, obiettivo e dubbi che avevamo in principio)
  2.1 Stato dell'arte - Math Melodies e limiti
  2.2 Stato dell'arte - tecnologie multi piattaforma
  2.3 Analisi degli attori

3. Progettazione (quali possibilità abbiamo valutato e su quali basi siamo giunti alle conclusioni)
  3.1 Design adattivo
  3.2 Design inclusivo
  3.3 Problemi tecnici affrontati
  3.4 Progettazione delle interfacce grafiche (schema di navigazione e dettaglio delle schermate principali)

4. Implementazione e testing
  4.1 Struttura e gestione della navigazione
  4.2 Modulo nativo per rilevazione del focus di accessibilità
  4.3 Riproduzione sequenziale di suoni asincroni
  4.4 Metodologia di validazione
  4.5 Risutati ottenuti

5. Conclusione e sviluppi futuri
  5.1 Conclusioni
  5.2 Sviluppi futuri
