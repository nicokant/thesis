\chapter{Progettazione}
\label{cap3}
In questo Capitolo viene trattata la progettazione di Math Melodies 2 illustrando le idee con cui la soluzione sarà poi implementata. Nella Sezione 3.1 si tratta del design inclusivo e delle scelte fatte per portare il maggior beneficio possibile a tutti gli utenti. Nella Sezione 3.2 saranno esposte le scelte con Math Melodies 2 effettuate per garantire l'usabilità tramite qualsiasi dispositivo. Nella Sezione 3.3 saranno esposti i problemi tecnici incontrati in seguito alla precedente analisi della tecnologia React Native. Infine la Sezione 3.4 mostrerà come sono state progettate le interfacce grafiche.

\section{Design inclusivo}
L'idea sottostante la progettazione dell'applicativo ha le sue radici nelle comuni esigenze dei diversi attori che ne faranno uso. È stato quindi mantenuto un aspetto grafico accattivante con molte immagini e colori, indispensabile per invogliare gli studenti normovedenti. Non si è però trascurata la cura per i dettagli che ne permettano l'accessibilità a utenti con disabilità visive.

\paragraph{}
Per favorire l'esplorazione Math Melodies 2 è stata progettata per sfruttare il maggior numero di pixel a disposizione sullo  schermo, senza nascondere alcun elemento dell'interfaccia alla vista. Infatti nessuna schermata ha elementi scorrevoli, l'unica eccezione è presentata dalla lista dei singoli esercizi che segue però un pattern consolidato e utilizzato già all'interno dello stesso sistema operativo. Questa necessità ha conseguenze evidenti soprattutto per quanto concerne dispositivi mobili di dimensioni ridotte.

\paragraph{}
\begin{wrapfigure}{r}{5.5cm}
	\centering
	\caption{difficoltà a raggiungere zone dello schermo [\href{http://bit.ly/mobile-desing}{realizzato da venturebeat.com}]}
	\label{figura:location}
	\includegraphics[width=5.5cm]{images/mobile-button-location}
\end{wrapfigure}

È importante dare dei punti di riferimento agli utenti: per questo i bottoni utilizzati all'interno delle interfacce si trovano tutti nel medesimo punto dello schermo. Si tratta di un pattern molto importante soprattutto per gli utenti non vedenti, che in questo modo non sono costretti esplorare l'intero schermo per trovare i pulsanti fondamentali. Questi pulsanti emergono al di sopra del resto dell'interfaccia e sono situati nell'angolo in basso a destra, anzichè nell'angolo in alto come avveniva in Math Melodies. Il vantaggio è soprattutto su smartphone, dove spesso gli utenti utilizzano solo la mano con cui sorreggono il dispositivo e sono quindi facilitati nel raggiungere gli angoli inferiori come illustrato in figura \ref{figura:location}.


\paragraph{}
Come già citato nella precedente analisi, le immagini utilizzate come sfondi nelle storie di Math Melodies sono ricche di molti dettagli e colori, un fattore che aiuta il coinvolgimento di utenti normovedenti ma che al tempo stesso, a causa della natura di alcune forme di ipovisione, può creare difficoltà nella lettura.
Math Melodies 2 risolve questo problema utilizzando uno sfondo unico per la lettura dei testi e mostrando immagini diverse solo durante un cambio di ambientazione della narrazione. Ne risulta che nel primo capitolo della storia vengono utilizzate solo cinque immagini. Avendo come obiettivo l'inclusione si è preferito un approccio omogeneo a tutti gli utenti, anzichè adottare soluzioni diverse per le singole tipologie di attori.  
 
\paragraph{}
Di questo approccio si avvale anche l'utilizzo che fa l'applicazione del sintetizzatore vocale per aiutare lo studente nella lettura della storia. Infatti ogni pagina della narrazione ---dove per pagina si intende il cambiamento di schermata--- viene letta tramite il sintetizzatore non appena l'utente la visualizza. Questa scelta, già presente in Math Melodies, è stata soggetta a rivalutazione durante la progettazione, in quanto forniva una funzionalità che gli utenti che utilizzano VoiceOver e TalkBack avevano già a disposizione. I motivi che hanno portato infine alla sua reintroduzione sono molteplici; da una parte gli studenti che frequentano la prima classe della scuola primaria e che ancora non padroneggiano la lettura sono in grado di seguire la narrazione, dall'altro il sintetizzatore vocale è un ausilio importante anche per quanto concerne studenti con disturbi specifici dell'apprendimento.  

\section{Design adattivo}
Il design adattivo è un approccio finalizzato all'organizzazione degli elementi di una stessa interfaccia grafica mostrata su dispositivi aventi dimensioni diverse tra loro. A differenza dell'approccio responsivo in cui il layout varia a seconda della larghezza dello schermo, il design adattivo stabilisce a priori un set di layout disponibili e utilizza per ciascun dispositivo il più consono. Il design responsive è maggiormente utilizzato in ambito web, dove è necessaria la fluidità dell'interfaccia e del contenuto indipendentemente dal dispositivo. Per quanto concerne invece le applicazioni per dispositivi mobili, l'approccio adattivo è utilizzato con maggiore frequenza sia per la natura prefissata delle interfacce per dispositivi mobili, sia perchè in grado di garantire un'esperienza utente su misura.

Math Melodies 2 è progettata per utilizzare due tipologie di griglie in grado di interagire tra loro. La prima \textbf{griglia}, detta \textbf{adattabile}, opera in modo tale da assegnare a ciascuna cella quadrata la lunghezza massimale dei suoi lati affinché tutta la griglia sia sempre contenuta nella medesima schermata. La seconda invece è in grado di operare una riorganizzazione delle celle a seconda dell'orientamento del dispositivo e per questo motivo si farà riferimento alla locuzione \textbf{griglia orientata}.

La griglia adattabile è esprimibile formalmente attraverso una funzione $grid(x,y)$ che associa a $x$ e $y$, che corrispondono alle dimensioni dello schermo, e i parametri $r$ e $c$, che indicano da quante righe e colonne la griglia è composta, il valore minimo tra il rapporto della larghezza per il numero di colonne e il rapporto tra altezza e numero di righe. In questo modo è possibile ottenere il valore che il lato di un quadrato dovrebbe avere affinché sia possibile comporre una griglia di $r$ righe e $c$ colonne in grado di occupare il maggior spazio possibile. 
\begin{equation}
	\label{math:adapt}
	grid(x,y) = min\bigg(\frac{x}{c},\frac{y}{r}\bigg)
\end{equation}


Per quanto riguarda l'aspetto implementativo, che sarà discusso per esteso nel Capitolo 4, è importante notare che la griglia orientata è stata sviluppata in modo da utilizzare al suo interno il componente adattivo ed estenderne il comportamento permettendo di specificare il numero di righe e colonne per ciascun orientamento anziché mantenere un aspetto prefissato. 
\paragraph{}
Tra le componenti che fanno utilizzo delle griglie una tra le piú importanti è la tastiera virtuale. La tastiera è infatti progettata per essere un'unica riga composta da dodici colonne quando il dispositivo è in orientamento \textit{landscape}, in modo da sfruttare tutta la larghezza del dispositivo. Se invece il device si trova orientato in \textit{portrait} la tastiera utilizza una griglia composta da due righe e sei colonne.

È possibile stimare quali siano i parametri migliori per comporre una tastiera di dodici pulsanti analiticamente studiando il rapporto tra l'altezza del dispositivo $y$ e l'altezza della tastiera $y_k$ generata tramite la funzione griglia adattiva \ref{math:adapt}. L'altezza della tastiera è data da:
\begin{equation}
	\label{math:hkey}
	y_k = \frac{x \cdot r_k}{c_k}
\end{equation}

e di conseguenza il rapporto tra le altezze è:
\begin{equation}
	R = \frac{y_k}{y} = \frac{x \cdot r_k}{y \cdot c_k}
\end{equation}


\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline 
		\textbf{dispositivo} & \textbf{aspect ratio} & \textbf{rows} & \textbf{columns} & $\mathbf{R}$ \\
		\hline 
		smartphone & 9:16 & 1 & 12 & 0.04 \\
		portrait & & 2 & 6 & 0.19  \\
		 & & 3 & 4 & 0.42 \\
		\hline
		smartphone & 16:9 & 1 & 12 & 0.15 \\
		landscape & & 2 & 6 & 0.59 \\
		& & 3 & 4 & 1.33 \\
		\hline
		tablet & 3:4 &  1 & 12 & 0.12 \\
		portrait & &  2 & 6 & 0.25 \\
		 & & 3 & 4 & 0.56 \\
		\hline
		tablet & 4:3 & 1 & 12 & 0.11 \\
		landscape & & 2 & 6 & 0.44 \\
		 & & 3 & 4 & 1.00 \\
		\hline
	\end{tabular}
	\caption{Rapporto spazio tastiera a seconda dell'aspect ratio e della disposizione}
	\label{table:ratio}
\end{table}

In tabella \ref{table:ratio} sono mostrati i valori di $R$ al variare dell'aspect ratio del dispositivo e della configurazione di una tastiera a dodici pulsanti. Stabilendo come soglia di accettabilità il 20\% di rapporto tra spazio e tastiera, questa analisi ci permette di individuare quale disposizione risulta più adatta per ciascun device. In particolare emerge che per dispositivi con orientamento portrait è preferibile utilizzare una griglia di due righe per sei colonne, mentre ruotando il dispositivo la soluzione più vicina alla soglia stabilita è una griglia con un'unica riga.  

\paragraph{}
Tuttavia queste informazioni non sono in grado di garantire che all'interno di schermate che fanno utilizzo sia di una tastiera sia di una griglia adattiva non ci siano delle sovrapposizioni tra questi elementi. È possibile a questo punto riscrivere la funzione griglia \ref{math:adapt} in modo da tenere conto della presenza della tastiera.

\begin{equation}
\label{math:both}
grid_{k}(x,y) = min\bigg(\frac{x}{c},\frac{y - y_k}{r}\bigg)
\end{equation}

È importante notare che utilizzando layout diversi della tastiera un cambiamento di orientamento del dispositivo non implica solamente l'inversione dell'aspect ratio ---quindi dei valori $x$ e $y$--- ma anche una variazione dei parametri $r_k$ e $c_k$. Sarà quindi necessario ricalcolare prima $y_k$ e in solo in seguito valutare $grid_k$ per ciascuna rotazione.
 
\section{Progettazione delle interfacce grafiche}
Avendo introdotto i concetti di griglia adattiva e orientata, in questa sezione saranno mostrate le applicazioni di tali concetti alle interfacce grafiche di cui è composta l'applicazione.

La prima schermata con cui l'utente viene a contatto è la home page dell'applicazione, composta da tre blocchi che attivano ciascuno una modalità di fruizione. In aggiunta ai già citati \textbf{Storia} e \textbf{Esercizi} è presente anche un pulsante originariamente dedicato alle informazioni per gli adulti che in questo studio non sarà trattato. La home page utilizza una griglia orientata per sfruttare al meglio l'asse di maggiore lunghezza, orientando verticalmente tale asse la pagina si disporrà in colonna mentre si posizionerà in riga quando l'orientamento è orizzontale.
\begin{figure}
	\centering
	\includegraphics[height=2.5in]{images/home_vert}
	\hspace*{.2in}
	\raisebox{0.25\height}{\includegraphics[width=3in]{images/home_hor}}
	\caption{Schermata Home di Math Melodies 2}
\end{figure}
    
\paragraph{}
Scegliendo Storia viene avviata la narrazione del primo capitolo di gioco. La modalità Storia è l'unica che presenta immagini di sfondo variabili in base al contesto della narrazione. La questione delle immagini di sfondo utilizzate da Math Melodies e della scelta di limitare la loro presenza, già trattata in precedenza, per quanto semplifichi la lettura per ipovedenti, non rappresenta una soluzione alla dipendenza delle immagini dall'aspect ratio del dispositivo.

Math Melodies 2 utilizza un set di immagini di sfondo per ciascun orientamento, le immagini verticali sono ottenute tramite sezioni dello sfondo orizzontale calcolando l'inverso del rapporto originale alla massima risoluzione, come mostrato in figura \ref{fig:bed}.
\begin{figure}
	\centering
	\includegraphics[height=2.5in]{images/story_vert}
	\hspace*{.2in}
	\raisebox{0.25\height}{\includegraphics[width=3in]{images/story_hor}}
	\caption{Schermata d'ambientazione in diversi orientamenti}
	\label{fig:bed}
\end{figure}

\paragraph{}
Scegliendo invece la modalità Esercizi si ha a disposizione la lista completa dei compiti eseguibili e una volta scelto il livello di difficoltà si può iniziare a svolgerlo.

Per rendere usabile su smartphone la pagina di esercizio è stato necessario operare una suddivisione dell'interfaccia di Math Melodies in schermate separate. Tutti gli esercizi indipendentemente dalla tipologia hanno in comune quattro schermate di navigazione: 
\begin{itemize}
	\item assegnazione del compito;
	\item svolgimento dell'esercizio;
	\item risposta corretta;
	\item risposta errata.
\end{itemize}
In aggiunta ai precedenti per gli esercizi della categoria \textit{risposta multipla} si è scelto di utilizzare uno schermo separato per mostrare la lista di possibili risposte tra cui scegliere anziché una finestra in sovraimpressione. Un esempio di esercizio completo di tutte le schermate è mostrato in figura \ref{fig:excount}.

\begin{figure}
	\centering
	\includegraphics[width=1.1in]{images/ex01}
	\includegraphics[width=1.1in]{images/ex02}
	\includegraphics[width=1.1in]{images/ex03}
	\includegraphics[width=1.1in]{images/ex04}
	\includegraphics[width=1.1in]{images/ex05}
	\caption{Esercizio a risposte multiple di conteggio}
	\label{fig:excount}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=3in]{images/complete-navigation}
	\caption{Navigazione all'interno dell'applicazione}
	\label{fig:nav}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=2.5in]{images/navigation}
	\caption{Navigazione all'interno di un esercizio}
	\label{fig:exnav}
\end{figure}


Tali schermate sono collegate tra di loro tramite la modalità di navigazione illustrata dal diagramma di flusso in figura \ref{fig:exnav}. 

Gli esercizi sono l'entità chiave dell'applicazione che infatti è stata strutturata in modo da portare sempre lo studente a svolgere un compito. Questo si può osservare nello schema di navigazione dell'intera applicazione in figura \ref{fig:nav}.

\clearpage
\section{Progettazione delle interazioni}
Per quanto concerne i dispositivi mobili il principale strumento di interazione è il touchscreen; a esso possono essere associate determinate azioni e gesti compiuti dagli utenti con l'ausilio di una o più dita. Math Melodies 2 utilizza esclusivamente il tocco, e non prevede alcun tipo di gesto nella risoluzione degli esercizi. Alcune \textit{gesture} risultano infatti sia poco intuitive per gli utenti normovedenti, a cui solitamente viene mostrato un suggerimento su come utilizzare tali gesture, sia poco accessibili per utenti con disabilità visive o motorie.

In particolare riguardo gli utenti non vedenti sarebbe necessario fornire un feedback progressivo tramite la sonificazione come già avviene per pattern noti del sistema operativo come lo \textit{scrolling}. Inoltre le gesture si utilizzano per dare accesso all'utente a porzioni di interfaccia che sarebbero altrimenti nascoste e quindi non direttamente esplorabili per utenti non vedenti.

\paragraph{}
Per trattare più approfonditamente le interazioni predisposte è necessario fare un'ulteriore classificazione degli esercizi e individuare i pattern implementati. È possibile definire quindi tre classi di interazione, che hanno una correlazione con la categoria a cui tali esercizi appartengono.

\begin{figure}
	\centering
	\includegraphics[height=2.3in]{images/compare}
	\hspace*{.2in}
	\includegraphics[height=2.3in]{images/complete}
	\hspace*{.2in}
	\includegraphics[height=2.3in]{images/toggle}
	\caption{Esercizi in classi di interazione}
	\label{fig:classes}
\end{figure}

\paragraph{}
La prima classe è detta \textbf{esplorativa} e fa generamente uso di oggetti e animali con un feedback sia visivo, realizzato tramite opacità, sia sonoro, ottenuto grazie alla riproduzione di un suono facilmente associabile all'oggetto. Si pone però il problema dell'univocità di oggetti uguali per utenti non vedenti, che è estramamente rilevante soprattutto negli esercizi che implicano il conteggio. La percezione sensoriale della propriocezione, che è la capacità di riconoscere la posizione del proprio corpo nello spazio, è fondamentale ma non sufficiente; è stato quindi necessario fornire una modalità di interazione che esibisse a tali utenti dei suggerimenti in grado di supportarli nella risoluzione del compito. Tramite il lettore di schermo le persone non vedenti sono in grado esplorare spostando il dito sul touchscreen oppure tramite una gesture di scorrerlo sequenzialmente elemento per elemento.

Math Melodies 2 ---tramite il plug in nativo sviluppato per Android--- riproduce il suono associato a un oggetto durante l'esplorazione dell'interfaccia se l'utente utilizza VoiceOver o TalkBack, ma lo fa solo la prima volta che l'utente vi pone il \textit{focus}. Successivamente tale oggetto sarà annunciato come \textit{già ascoltato} e sarà chiesto all'utente di eseguire un doppio tocco sullo schermo per ascoltarlo nuovamente.

\paragraph{}
La seconda classe di interazione detta \textbf{numerica} contempla gli esercizi con inserimento tramite tastiera di numeri per completare una o più operazioni. Questi esercizi prevedono appunto una tastiera a comparsa che risulta utilizzabile solo quando l'utente desidera inserire un numero in una casella vuota. Math Melodies 2 utilizza un feedback uditivo per notificare all'utente il cambiamento nell'interfaccia e la presenza o assenza della tastiera. Inoltre essendo sia l'esercizio sia la tastiera composti di numeri, si ripropone il problema di duplicità di elementi uguali già discusso nel precedente paragrafo. Si è scelto quindi di annunciare gli elementi della tastiera prefissando l'azione ---\textit{inserisci}--- eseguita dal pulsante in modo da risolvere l'ambiguità.

\paragraph{}
L'ultima classe infine è detta \textbf{commutativa} perchè utilizza il pattern di \textit{toggling}. L'utente è in grado di aggiungere un oggetto se non è presente o di rimuoverlo se invece lo è tramite un doppio tocco, mentre con un singolo tocco sarà riprodotto il suono dell'oggetto. Queste interazioni sono difficilmente integrabili con il lettore di schermo che sovrascrive tali gesture con delle proprie, motivo per cui si è deciso di adottare una semplificazione per migliorarne l'accessibilità. Quando l'utente non vedente pone il \textit{focus} del lettore su un oggetto, non solo viene emesso il suono corrispondente, ma l'applicazione fornisce in aggiunta il suggerimento di eseguire un doppio tocco per rimuovere l'oggetto. Se invece l'oggetto non è presente viene annunciato all'utente di premere due volte sullo schermo per aggiungerlo e nel momento in cui esso viene aggiunto viene riprodotto anche il suono corrispondente in modo da fornire un feedback.